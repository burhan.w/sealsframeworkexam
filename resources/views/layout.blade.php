<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>@yield('title')</title>
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
       
        <link rel="stylesheet" href="@relative('css/bootstrap.min.css')">

        <link rel="stylesheet" href="@relative('css/dataTables.bootstrap4.min.css')">
        <link rel="stylesheet" href="@relative('css/buttons.bootstrap4.min.css')">

        <link rel="stylesheet" href="@relative('css/flatpickr.min.css')">
        
        @yield('style')
        
    </head>
    <body>

           
        {{-- include file แนบค่า name ไปด้วย --}}
        {{-- @include('sidebar', ['name'=>'Burhan'])  --}}
        {{-- include file  จะเช็ดว่ามีไฟล์ชื่อนี้ไหม ถ้าไม่มีจะไม่แสดง --}}
        {{-- @includeif('sidebar1', ['name'=>'Burhan']) --}}
        
        <div class="container">
            @yield('content') 
        </div>
        

        <script src="@relative('js/jquery.min.js')"></script>

        <script src="@relative('js/bootstrap.bundle.min.js')"></script>
        
        <script src="@relative('js/moment.min.js')"></script>
        
        <script src="@relative('js/jquery.dataTables.min.js')"></script>
        <script src="@relative('js/dataTables.bootstrap4.min.js')"></script>
        <script src="@relative('js/pdfmake.min.js')"></script>
        <script src="@relative('js/vfs_fonts.js')"></script>
        <script src="@relative('js/dataTables.buttons.min.js')"></script>
        <script src="@relative('js/buttons.bootstrap4.min.js')"></script>
        <script src="@relative('js/buttons.flash.min.js')"></script>
        <script src="@relative('js/buttons.html5.min.js')"></script>
        <script src="@relative('js/buttons.print.min.js')"></script>

        <script src="@relative('js/flatpickr.min.js')"></script>

        @yield('script')
    </body>
</html>