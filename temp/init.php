<?PHP 
    /**
    * Session Start
    */
    if(!isset($_SESSION)) session_start();

    /**
    * Global Constant
    */
    define('BASE_PATH', realpath(__DIR__.'/../'));
    // define('WWW_PATH', realpath(__DIR__.'/../') . "/wwwroot/");

    /**
    * Autoload
    */
    $loader = require_once BASE_PATH . '/vendor/autoload.php';

    /**
    * Load Environment
    */
    new \App\Extensions\Env(BASE_PATH);
    // echo getenv('APP_PATH');
    /**
    * Initial Database
    */
    // $entityManager = new \App\Classes\Database(BASE_PATH);

    /**
    * Initial Routing
    */
    $router = new AltoRouter();
    $router->setBasePath(getenv('APP_PATH'));
    require_once BASE_PATH.'/application/routers/web.php';
    // var_dump($router->match());
    // require_once BASE_PATH.'/app/routing/api.php';
    new \App\Extensions\RoutingDispatcher($router);