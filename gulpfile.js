var gulp = require ('gulp');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var minifycss = require('gulp-minify-css');

var jsFile = [
	'./resources/libs/jquery/dist/jquery.min.js', 
	'./resources/libs/bootstrap/dist/js/bootstrap.bundle.min.js',
	'./node_modules/moment/min/moment.min.js',
	// './resources/libs/jszip/dist/jszip.min.js',
	'./resources/libs/pdfmake/build/pdfmake.min.js',
	'./resources/libs/pdfmake/build/vfs_fonts.js',
	'./resources/libs/datatables.net-buttons/js/dataTables.buttons.min.js',
    './resources/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js',
    './resources/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
    './resources/libs/datatables.net/js/jquery.dataTables.min.js',
	'./resources/libs/datatables.net-buttons/js/buttons.flash.min.js',
	'./resources/libs/datatables.net-buttons/js/buttons.html5.min.js',
    './resources/libs/datatables.net-buttons/js/buttons.print.min.js',
    './node_modules/flatpickr/dist/flatpickr.min.js',
];

var cssFile = [
    './resources/libs/bootstrap/dist/css/bootstrap.min.css',
	'./resources/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
    './resources/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css',
    './node_modules/flatpickr/dist/flatpickr.min.css',
];

gulp.task ('libjs',function(){
	return gulp
	.src(jsFile) 
	.pipe(gulp.dest('./wwwroot/js'));
});
gulp.task ('libcss',function(){
	return gulp
	.src(cssFile) 
	.pipe(gulp.dest('./wwwroot/css'));
});

// gulp.task ('minjsDataTable',function(){
// 	return gulp
// 	.src('./resources/assets/js/dataTables.bootstrap4.js')
// 	.pipe(minifyJS())
// 	.pipe(rename({suffix: '.min'}))
// 	.pipe(gulp.dest('./resources/assets/js/'));
// });

// gulp.task ('libjsDataTable',function(){
// 	return gulp
// 	.src('./resources/assets/js/dataTables.bootstrap4.min.js') 
// 	.pipe(gulp.dest('./wwwroot/js'));
// });	

// gulp.task ('mincssDataTable',function(){
// 	return gulp
// 	.src('./resources/assets/css/dataTables.bootstrap4.css')
// 	.pipe(minifycss())
// 	.pipe(rename({suffix: '.min'}))
// 	.pipe(gulp.dest('./resources/assets/css/'));
// });

// gulp.task ('libcssDataTable',function(){
// 	return gulp
// 	.src('./resources/assets/css/dataTables.bootstrap4.min.css') 
// 	.pipe(gulp.dest('./wwwroot/css'));
// });	

// gulp.task ('minjspdfmake',function(){
// 	return gulp
// 	.src('./resources/assets/js/pdfmake.js')
// 	.pipe(minifyJS())
// 	.pipe(rename({suffix: '.min'}))
// 	.pipe(gulp.dest('./resources/assets/js/'));
// });

// gulp.task ('libjspdfmake',function(){
// 	return gulp
// 	.src('./resources/assets/js/pdfmake.min.js')
// 	.pipe(gulp.dest('./wwwroot/js'));
// });	

// gulp.task ('libjsvfsFonts',function(){
// 	return gulp
// 	.src('./resources/assets/js/vfs_fonts.js')
// 	.pipe(gulp.dest('./wwwroot/js'));
// });


// flatpickr
// gulp.task ('minPluginRageFlatpickr',function(){
// 	return gulp
// 	.src('./node_modules/flatpickr/dist/plugins/rangePlugin.js')
// 	.pipe(minifyJS())
// 	.pipe(rename({suffix: '.min'}))
// 	.pipe(gulp.dest('./wwwroot/js'));
// });	

gulp.task('all',gulp.series('libjs','libcss'));