<?PHP 
    namespace App\Extensions;
    
    class Env {
    	public function __construct($p_base_path) {
            $dotEnv = \Dotenv\Dotenv::create($p_base_path);
            $dotEnv->load();
        }
    }
    