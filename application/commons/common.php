<?PHP 
    use eftec\bladeone;

    class myBlade extends bladeone\BladeOne{
        // use eftec\bladeone\BladeOneHtml;
        use eftec\bladeone\BladeOneHtmlBootstrap;

        protected function compileMyFunction($expresion){
            
            $regx = "/\('(.*)'\)/";
            preg_match($regx, $expresion, $matches);
            var_dump($matches);
            return $this->phpTag."echo \"<input type='text' value='$matches[1]' /> \"; ?> \n";
            
        }
    }

    function view($path, array $data = [])
    {
        $view = __DIR__ . '/../../resources/views';
        $cache = __DIR__ . '/../../temp/caches';
        
        // $blade = new bladeone\BladeOne($view, $cache, 0);
        // upd; 620730
        $blade = new myBlade($view, $cache);
        
        $blade->setBaseUrl(getenv('APP_URL'));
        // $blade->addAssetDict('js/jquery','https://code.jquery.com/jquery-3.3.1.min.js'); //กำหนด alias

        echo $blade->run($path,$data);
    }

    function route($url){
        $strAppPath = getenv('APP_PATH');
    	$strLastChr = substr($strAppPath, strlen($strAppPath), 1);      //get last character
        if ($strLastChr != "/") {
            $strAppPath .= "/";
        }
    	return $strAppPath . $url;
    }
