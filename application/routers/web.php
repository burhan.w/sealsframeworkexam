<?PHP 
    // $router->map('GET','/home','target','name');

    
    $router->map('GET', '/[a:controller]', 'App\Controllers\{controller}Controller@index', 'default-get');
    $router->map('GET', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-get2');

    $router->map('POST', '/[a:controller]/[a:method]', 'App\Controllers\{controller}Controller@{method}', 'default-post1');
    
    /**
     * route image
     */
    $router->map('GET', '/[a:controller]/[a:method]/[*:star]?', 'App\Controllers\{controller}Controller@{method}', 'default-get3'); 
    

    $router->map('GET','/admin/user/lists','App\Controllers\HomeController@Lists','get-list3');
    $router->map('GET','/admin/user/get/[i:id]','App\Controllers\HomeController@Get','get-list4');
    // $router->map('GET','/','App\Controllers\TestController@index','get-get5');   //Rout

    $router->map('DELETE', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-delete');
    // $router->map('GET', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-get2');
    // $router->map('GET', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-get2');